from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm


# Create your views here.
@login_required
def receipt_list(request):
    receipt_items = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_items": receipt_items}
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)


def category_list(request):
    category_items = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_items": category_items,
    }
    return render(request, "receipts/categories_list.html", context)


def account_list(request):
    account_items = Account.objects.filter(owner=request.user)
    context = {"account_items": account_items}
    return render(request, "receipts/account_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            expense = form.save(False)
            expense.owner = request.user
            expense.save()
            return redirect("category_list")
    else:
        form = ExpenseForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create_account.html", context)
